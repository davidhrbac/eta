function getWorkDaysCount(endtime)
{
   var d1 = new Date();
   var d2 = endtime;
   var flag=true;
   var day,daycount=0;
   while(flag)
   {
      day=d1.getDay();
      if(day != 0 && day != 6)
         daycount++;
      d1.setDate(d1.getDate()+1) ;
      if(d1.getDate() == d2.getDate() &&
                  d1.getMonth()== d2.getMonth())
      {
          flag=false;
      }
 }
return daycount;
}


function getTimeRemaining(endtime) {
  const total = Date.parse(endtime) - Date.parse(new Date());
  const seconds = Math.floor((total / 1000) % 60);
  const minutes = Math.floor((total / 1000 / 60) % 60);
  const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
  const days = Math.floor(total / (1000 * 60 * 60 * 24));
  const tdays = Math.ceil(total / (1000 * 60 * 60 * 24));
  return {
    total,
    days,
    hours,
    minutes,
    seconds,
    tdays
  };
}

function initializeClock(id, endtime) {
  const clock = document.getElementById(id);
  const daysSpan = clock.querySelector('.days');
  const hoursSpan = clock.querySelector('.hours');
  const minutesSpan = clock.querySelector('.minutes');
  const secondsSpan = clock.querySelector('.seconds');

  const tdaysSpan = clock.querySelector('.tdays');
  const twdaysSpan = clock.querySelector('.twdays');

  function updateClock() {
    const t = getTimeRemaining(endtime);
    const tw = getWorkDaysCount(endtime);

    daysSpan.innerHTML = t.days;
    hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);
    tdaysSpan.innerHTML = "T -" + t.tdays;
    twdaysSpan.innerHTML = "T -" + tw;

    if (t.total <= 0) {
      clearInterval(timeinterval);
    }
  }

  updateClock();
  const timeinterval = setInterval(updateClock, 1000);
}

const deadline = new Date("Feb 1, 2021 00:00:00");
// initializeClock('clockdiv', deadline);
